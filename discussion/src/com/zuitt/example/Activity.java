package com.zuitt.example;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter first name:");
        String firstName = myObj.nextLine();
        System.out.println("Enter last name:");
        String lastName = myObj.nextLine();
        System.out.println("Enter first subject grade:");
        double firstGrade = myObj.nextDouble();
        System.out.println("Enter second subject grade:");
        double secondGrade = myObj.nextDouble();
        System.out.println("Enter third subject grade:");
        double thirdGrade = myObj.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + ((firstGrade + secondGrade + thirdGrade)/3));






    }
}
